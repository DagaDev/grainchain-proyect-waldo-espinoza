# Prueba de logica para Grainchain

## Explicacion de documentos

-  `habitaciones.txt`: Documento donde se encuentra la matriz de unos y ceros.
-  `plantillaMatriz.html`: Plantilla para creacion de matriz en pdf.
-  `matriz.pdf`: Matriz exportada en pdf para mayor comodidad.


## Pasos para correr el programa exitosamente

- `npm install`: Instalar dependencias.
- `npm start`: Correr sistema para generar matriz.



