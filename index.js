const fs = require('fs');
var pdf = require('html-pdf');
const handlebars = require("handlebars");
const path = require("path");

const main = async () => {
    try {
        const data = fs.readFileSync(path.join(process.cwd(), '/habitaciones.txt'), 'utf8')
        if (data === '')
            throw new Error('No matrix found')

        const arrayFile = data.split(/\r\n|\r|\n/)
        const matriz = [];

        for (let index = 0; index < arrayFile.length; index++) {
            const element = arrayFile[index];
            const separateArray = element.split('');
            matriz[index] = [];
            for (let x = 0; x < separateArray.length; x++) {
                const element = separateArray[x];
                if(element !== '0' && element !== '1')
                    throw new Error('Your matrix is ​​not in the correct format')

                matriz[index][x] = element
            }
        }
        const emailTemplateSource = await fs.readFileSync(path.join(process.cwd(),"plantillaMatriz.html"), "utf8")
        if (emailTemplateSource === '')
            throw new Error('No plantilla found')

        console.log('Creando la mejor matriz...')
        handlebars.registerHelper('isOff', function (value) {
            return value === '0';
          });
        var template = handlebars.compile(emailTemplateSource);
        var result = template({
            matriz
        });

        console.log('Conviertiendo matriz en pdf...')
        pdf.create(result).toFile(path.join(process.cwd(),'matriz.pdf'), function (err, res) {
            if (err)
                throw new Error(err)

            console.log('¡Matriz terminada!')
            console.log('¡Puedes visualizar tu documento en esta carpeta con el nombre de matriz.pdf!')
        });


    } catch (err) {
        console.log(err)
    }
}


main();